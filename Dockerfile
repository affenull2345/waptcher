FROM node:latest
VOLUME /home/patcher/bundles/
WORKDIR /home/patcher/
COPY ./ /home/patcher/
RUN yarn
RUN apt update -y && apt install -y zip
ENTRYPOINT /home/patcher/patch.sh
