#!/bin/sh

APPNAME='whatsapp'
APPPKG=$PWD/$APPNAME.zip
APPDIR=$PWD/$APPNAME
CONTACT='Affe Null (email: affenull2345@gmail.com)'
BUNDLEDIR=$PWD/bundles
MFPATH=$BUNDLEDIR/manifest-latest.json
MFURL="http://89.58.28.240/waptcher/latest-bundle-$APPNAME.zip"

set -e

_oops () {
  printf "\033[47;30m>>>\033[41;37m %s\033[m\n" "Oops: $*"
}

_oops_die () {
  _oops "$@"
  trap "" EXIT
  exit 1
}

_info () {
  printf "\033[47;30m>>>\033[44;37m %s\033[m\n" "Info: $*"
}

_dbg () {
  printf "\033[100;30m>>>> %s\033[m\n" "$*"
}

trap "_oops 'An error occurred'" EXIT

APP_VERSION=0

if [ -z "$NOVERSIONCHECK" ] && [ -e "$MFPATH" ]; then
  APP_VERSION=$(sed -E -z 's/^.*"version":\s*"([^"]*)".*$/\1/' \
    "$MFPATH")
  _dbg "Current version: $APP_VERSION"
fi

export APP_VERSION

_info "Starting Node.js to download "$APPNAME" from KaiStore"
node .
[ -f "$APPPKG" ] || _oops_die "Newer version unavailable"

_info "Re-unpacking $APPNAME from $APPPKG"
rm -rf "$APPDIR"
unzip -d "$APPDIR" "$APPPKG"
_info "Patching"

cd "$APPDIR"

_info "(1/4) remove META-INF and other files"
rm -rf META-INF .KaiAds.appinfo.json

_info "(2/4) fallback to mozIntl"
sed -i 's/!\([^()]*Intl\))[^;]*;/!\1)\1=window.mozIntl;/' page.js

_info "(3/4) re-theme"
find . -regex '.*\.\(js\|html\|css\)' \
  -exec printf '>>>> patching %s\n' {} \; \
  -exec sed -i -E -z 's/#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/#\3\2\1/' {} \;

_info "(4/4) remove references to store, add Affe Null contact info"
for file in langs/*; do
  _dbg "Patching $file"
  mv "$file" "$file.orig"
  sed \
    -e 's/"[^"]*W\\ufeffh[^"]*Store[^"]*"/"Please update your version of WhatsApp. If this error persists, please contact '"$CONTACT"'."/i' \
    -e 's/"[^"]*PDF[^"]*\(Store\|Kai\)[^"]*"/"Please install a PDF reader."/i' \
    "$file.orig" | sed \
    's/"[^"]*Store[^"]*"/"No supported app store could be opened. Please install the app manually."/i' > "$file"
done

_info "Making bundle"

mkdir -p "$BUNDLEDIR"

cp manifest.webapp "$MFPATH"
zip -r "$BUNDLEDIR/application.zip" .
cd "$BUNDLEDIR"
cat > metadata.json <<END
{"version":1,"manifestURL":"$MFURL"}
END
zip "latest-bundle-$APPNAME" metadata.json application.zip

_info "Done"

trap "" EXIT
