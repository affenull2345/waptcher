const fs = require('fs');
const compareVersions = require('compare-versions');
const Requester = require('./kaistone-requester');

const C_SOCIAL = 30;

const settings = {
  dev: {
    model: 'GoFlip2',
    imei: '123456789012345',
    type: 999999,
    brand: 'AlcatelOneTouch',
    os: 'KaiOS',
    version: '2.5.4',
    ua: 'Mozilla/5.0 (Mobile; GoFlip2; rv:48.0) Gecko/48.0 Firefox/48.0 KAIOS/2.5.4',
    cu: '4044O-2BAQUS1-R',
    mcc: '0',
    mnc: '0'
  },
  api: {
    app: {
      id: 'CAlTn_6yQsgyJKrr-nCh',
      name: 'KaiOS Plus',
      ver: '2.5.4'
    },
    server: {
      url: 'https://api.kaiostech.com'
    },
    ver: '3.0'
  },
  auth: {
    method: 'api-key',
    key: 'baJ_nea27HqSskijhZlT'
  }
};

const save_file = (data) => {
  console.log('Saving package file...');
  fs.writeFile('whatsapp.zip', data, (err) => {
    if(err) throw err;
    else console.log('Done');
  });
};

new Requester(settings.auth, settings.api, settings.dev, (err) => {
  throw err;
}, function(){
  console.log('Requesting app list...');
  this.send({
    method: 'GET',
    path: `/kc_ksfe/v1.0/apps?os=${settings.dev.version}&category=${C_SOCIAL}&mcc=null&mnc=null&bookmark=false`,
    type: 'json'
  }).then((data) => {
    const wa = data.apps.find((app) => app.name === 'whatsapp');
    if(process.env.APP_VERSION){
      if(compareVersions(wa.version, process.env.APP_VERSION) <= 0){
        console.log('Version is new enough. Exiting.');
        return;
      }
    }
    console.log('Downloading package...');
    this.send({
      method: 'GET',
      path: wa.package_path,
      type: 'buffer'
    }).then(save_file);
  });
});
